import { USER_LOGGED } from '../constants/user';

const INITIAL_STATE = {
  userLogged: false,
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_LOGGED:
      return {
        ...state,
        userLogged: true,
      };
    default:
      return state;
  }
};

export default userReducer;
