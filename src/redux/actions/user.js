import { USER_LOGGED } from '../constants/user';

export const setUserLogged = () => ({
  type: USER_LOGGED,
});
