module.exports = {
  setupFiles: ['./setupTests.js'],
  transformIgnorePatterns: ['./node_modules/'],
  testRegex: '((\\.|/*.)(test))\\.js?$',
  coverageDirectory: './coverage',
  collectCoverage: true,
  collectCoverageFrom: [
    '**/src/**',
    '!**/node_modules/**',
    '!**/build/**',
    '!**/dist/**',
    '!**/coverage/**',
  ],
  coverageReporters: ['text', 'text-summary', 'lcov'],
  testResultsProcessor: 'jest-sonar-reporter',
};
